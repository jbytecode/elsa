#include "hints_base.h"

#include "DataContainer.h"
#include "Descriptors/CurvedDetectorDescriptor.h"
#include "Descriptors/DetectorDescriptor.h"
#include "Descriptors/PlanarDetectorDescriptor.h"
#include "Descriptors/VolumeDescriptor.h"
#include "Descriptors/RandomBlocksDescriptor.h"
#include "LinearOperator.h"
#include "DescriptorUtils.h"
#include "Error.h"
#include "Geometry.h"

#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <functional>

namespace elsa
{
    namespace py = pybind11;

    class CoreHints : ModuleHints
    {
    private:
        /// wrapper for variadic functions
        template <typename T, std::size_t N, typename = std::make_index_sequence<N>>
        struct invokeBestCommonVariadic;

        template <typename T, std::size_t N, std::size_t... S>
        struct invokeBestCommonVariadic<T, N, std::index_sequence<S...>> {
            static auto exec(const py::args& args)
            {
                if (args.size() == N)
                    return elsa::bestCommon(args[S].template cast<T>()...);

                if constexpr (N > 1) {
                    return invokeBestCommonVariadic<T, N - 1>::exec(args);
                } else {
                    throw LogicError("Unsupported number of variadic arguments");
                }
            };
        };

    public:
        static void addCustomFunctions(py::module& m)
        {
            m.def("adjoint", &adjointHelper<float>)
                .def("adjoint", &adjointHelper<double>)
                .def("adjoint", &adjointHelper<complex<float>>)
                .def("adjoint", &adjointHelper<complex<double>>);

            m.def("leaf", &leafHelper<float>)
                .def("leaf", &leafHelper<double>)
                .def("leaf", &leafHelper<complex<float>>)
                .def("leaf", &leafHelper<complex<double>>);

            m.def("bestCommon", (std::unique_ptr<DataDescriptor>(*)(
                                    const std::vector<const DataDescriptor*>&))(&bestCommon))
                .def("bestCommon", &invokeBestCommonVariadic<const DataDescriptor&, 10>::exec);
        }

    private:
        template <typename data_t>
        static LinearOperator<data_t> adjointHelper(const LinearOperator<data_t>& op)
        {
            return adjoint(op);
        }

        template <typename data_t>
        static LinearOperator<data_t> leafHelper(const LinearOperator<data_t>& op)
        {
            return leaf(op);
        }
    };

    template <typename data_t>
    class LinearOperatorHints : public ClassHints<elsa::LinearOperator<data_t>>
    {
    public:
        template <typename type_, typename... options>
        static void addCustomMethods(py::class_<type_, options...>& c)
        {
            c.def(py::self + py::self).def(py::self * py::self);
        }
    };

    template <typename data_t>
    class DetectorDescriptorHintsBase : public ClassHints<data_t>
    {
    public:
        constexpr static std::array ignoreMethods = {"computeRayFromDetectorCoord"};

        template <typename type_, typename... options>
        static void addCustomMethods(py::class_<type_, options...>& c)
        {
            c.def(
                "computeRayFromDetectorCoord",
                [](const DetectorDescriptor& desc, const index_t detectorIndex) {
                    auto result = desc.computeRayFromDetectorCoord(detectorIndex);
                    return std::make_tuple(result.origin(), result.direction());
                },
                py::arg("detectorIndex"));
            c.def(
                "computeRayFromDetectorCoord",
                [](const DetectorDescriptor& desc, const IndexVector_t coord) {
                    auto result = desc.computeRayFromDetectorCoord(coord);
                    return std::make_tuple(result.origin(), result.direction());
                },
                py::arg("coord"));
            c.def(
                "computeRayFromDetectorCoord",
                [](const DetectorDescriptor& desc, const RealVector_t& detectorCoord,
                   const index_t poseIndex) {
                    auto result = desc.computeRayFromDetectorCoord(detectorCoord, poseIndex);
                    return std::make_tuple(result.origin(), result.direction());
                },
                py::arg("detectorCoord"), py::arg("poseIndex"));
        }
    };

    class RandomBlocksDescriptorHints : public ClassHints<RandomBlocksDescriptor>
    {
    public:
        template <typename type_, typename... options>
        static void addCustomMethods(py::class_<type_, options...>& c)
        {
            // define custom constructors for the constructors accepting an rvalue reference
            c.def(py::init([](std::vector<const DataDescriptor*>& blockDescriptors) {
                std::vector<std::unique_ptr<DataDescriptor>> cloned;
                for (const auto op : blockDescriptors) {
                    cloned.push_back(op->clone());
                }

                return std::make_unique<RandomBlocksDescriptor>(std::move(cloned));
            }));
        }
    };

    template <typename TransparentClass>
    class TransparentClassHints : public ClassHints<TransparentClass>
    {
    public:
        using Vector = std::remove_reference_t<decltype(std::declval<TransparentClass>().get())>;
        using Scalar = decltype(std::declval<Vector>().sum());

        template <typename type_, typename... options>
        static void addCustomMethods(py::class_<type_, options...>& c)
        {
            c.def(py::init<Vector>())
                .def("__getitem__",
                     (Scalar(TransparentClass::*)(index_t i) const) & TransparentClass::operator[]);
        }
    };

    template class LinearOperatorHints<float>;
    template class LinearOperatorHints<double>;
    template class LinearOperatorHints<complex<float>>;
    template class LinearOperatorHints<complex<double>>;
    template class TransparentClassHints<geometry::Spacing2D>;
    template class TransparentClassHints<geometry::OriginShift2D>;
    template class TransparentClassHints<geometry::Coefficients<2>>;
    template class TransparentClassHints<geometry::Spacing3D>;
    template class TransparentClassHints<geometry::OriginShift3D>;
    template class TransparentClassHints<geometry::Coefficients<3>>;
} // namespace elsa
