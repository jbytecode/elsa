#include "utils.h"

namespace elsa
{

    template <typename data_t>
    data_t cubic_interpolation(data_t x0, data_t f0, data_t der_f0, data_t x1, data_t f1, data_t x2,
                               data_t f2)
    {
        data_t d1 = x1 - x0;
        data_t d2 = x2 - x0;
        data_t d1_2 = d1 * d1;
        data_t d2_2 = d2 * d2;
        data_t denominator = d1_2 * d2_2 * (d1 - d2);
        data_t r1 = f1 - f0 - der_f0 * d1;
        data_t r2 = f2 - f0 - der_f0 * d2;
        // TODO: check if denominator is 0 or very small
        data_t a = (d2_2 * r1 - d1_2 * r2) / denominator;
        data_t b = (-d2_2 * d2 * r1 + d1_2 * d1 * r2) / denominator;
        return x0 + (-b + std::sqrt(static_cast<data_t>(b * b - 3 * a * der_f0))) / (3 * a);
    }

    template float cubic_interpolation<float>(float x0, float f0, float der_f0, float x1, float f1,
                                              float x2, float f2);
    template double cubic_interpolation<double>(double x0, double f0, double der_f0, double x1,
                                                double f1, double x2, double f2);
} // namespace elsa
