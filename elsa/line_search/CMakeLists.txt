# list all the headers of the module
set(MODULE_HEADERS
    LineSearchMethod.h
    ArmijoCondition.h
    BarzilaiBorwein.h
    utils/utils.h
    GoldsteinCondition.h
    StrongWolfeCondition.h
    FixedStepSize.h
    SteepestDescentStepLS.h
    NewtonRaphson.h
)

# list all the code files of the module
set(MODULE_SOURCES
    LineSearchMethod.cpp
    ArmijoCondition.cpp
    BarzilaiBorwein.cpp
    utils/utils.cpp
    GoldsteinCondition.cpp
    StrongWolfeCondition.cpp
    FixedStepSize.cpp
    SteepestDescentStepLS.cpp
    NewtonRaphson.cpp
)

list(APPEND MODULE_PUBLIC_DEPS elsa_core elsa_functionals)
list(APPEND MODULE_PRIVATE_DEPS)

ADD_ELSA_MODULE(
    line_search "${MODULE_HEADERS}" "${MODULE_SOURCES}" INSTALL_DIR PUBLIC_DEPS ${MODULE_PUBLIC_DEPS}
    PRIVATE_DEPS ${MODULE_PRIVATE_DEPS}
)

write_module_config(${ELSA_MODULE_NAME} DEPENDENCIES elsa_core elsa_functionals)
