#include "Diagonal.h"
#include "Timer.h"
#include "TypeCasts.hpp"

namespace elsa
{
    template <typename data_t>
    Diagonal<data_t>::Diagonal(const DataDescriptor& domain, const DataDescriptor& range,
                               const OperatorList& ops)
        : LinearOperator<data_t>(domain, range)
    {
        ops_.reserve(ops.size());
        for (const auto& op : ops)
            ops_.push_back(op->clone());
    }

    template <typename data_t>
    void Diagonal<data_t>::applyImpl(const DataContainer<data_t>& x,
                                     DataContainer<data_t>& Ax) const
    {
        Timer timeguard("Diagonal", "apply");

        for (std::size_t i = 0; i < ops_.size(); ++i) {
            auto blk = Ax.getBlock(asSigned(i));
            ops_[i]->apply(x.getBlock(asSigned(i)), blk);
        }
    }

    template <typename data_t>
    void Diagonal<data_t>::applyAdjointImpl(const DataContainer<data_t>& y,
                                            DataContainer<data_t>& Aty) const
    {
        Timer timeguard("Diagonal", "applyAdjoint");

        for (std::size_t i = 0; i < ops_.size(); ++i) {
            auto blk = Aty.getBlock(asSigned(i));
            ops_[i]->applyAdjoint(y.getBlock(asSigned(i)), blk);
        }
    }

    template <typename data_t>
    Diagonal<data_t>* Diagonal<data_t>::cloneImpl() const
    {
        return new Diagonal(this->getDomainDescriptor(), this->getRangeDescriptor(), ops_);
    }

    template <typename data_t>
    bool Diagonal<data_t>::isEqual(const LinearOperator<data_t>& other) const
    {
        if (!LinearOperator<data_t>::isEqual(other))
            return false;

        return is<Diagonal>(other);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class Diagonal<float>;
    template class Diagonal<double>;

} // namespace elsa
