#include "CGNL.h"
#include "Logger.h"
#include "TypeCasts.hpp"
#include "spdlog/stopwatch.h"
#include "LineSearchMethod.h"

namespace elsa
{
    template <typename data_t>
    CGNL<data_t>::CGNL(const Functional<data_t>& functional,
                       const LineSearchMethod<data_t>& line_search_function)
        : Solver<data_t>(),
          f_{functional.clone()},
          r_(empty<data_t>(functional.getDomainDescriptor())),
          d_(empty<data_t>(functional.getDomainDescriptor())),
          delta_(0),
          deltaZero_(0),
          beta_(0),
          alpha_(0),
          lineSearch_{line_search_function.clone()},
          beta_function_{betaPolakRibiere}
    {
    }

    template <typename data_t>
    CGNL<data_t>::CGNL(const Functional<data_t>& functional,
                       const LineSearchMethod<data_t>& line_search_function,
                       const BetaFunction& beta_function)
        : Solver<data_t>(),
          f_{functional.clone()},
          r_(empty<data_t>(functional.getDomainDescriptor())),
          d_(empty<data_t>(functional.getDomainDescriptor())),
          delta_(0),
          deltaZero_(0),
          beta_(0),
          alpha_(0),
          lineSearch_{line_search_function.clone()},
          beta_function_{beta_function}
    {
    }

    template <typename data_t>
    DataContainer<data_t> CGNL<data_t>::setup(std::optional<DataContainer<data_t>> x0)
    {
        auto x = extract_or(x0, f_->getDomainDescriptor());

        // r <= -f'(x)
        r_ = data_t{-1.0} * f_->getGradient(x);
        d_ = r_;

        // delta <= r^T * d
        delta_ = r_.dot(d_);

        // deltaZero <= delta
        deltaZero_ = delta_;

        // restart every n
        restart_ = f_->getDomainDescriptor().getNumberOfCoefficients();

        this->name_ = "CGNL";

        // setup done!
        this->configured_ = true;

        return x;
    }

    template <typename data_t>
    DataContainer<data_t> CGNL<data_t>::step(DataContainer<data_t> x)
    {
        // line search
        alpha_ = lineSearch_->solve(x, d_);

        // update x
        x += alpha_ * d_;

        // beta function
        // r <= -f'(x)
        r_ = data_t{-1.0} * f_->getGradient(x);

        std::tie(beta_, delta_) = beta_function_(d_, r_, delta_);

        if (this->curiter_ % restart_ == 0 || beta_ <= 0.0) {
            d_ = r_;
        } else {
            d_ = r_ + beta_ * d_;
        }

        return x;
    }

    template <typename data_t>
    bool CGNL<data_t>::shouldStop() const
    {
        return delta_ <= epsilon_ * epsilon_ * deltaZero_;
    }

    template <typename data_t>
    std::string CGNL<data_t>::formatHeader() const
    {
        return fmt::format("{:^15} | {:^15} | {:^15}", "delta", "beta", "alpha");
    }

    template <typename data_t>
    std::string CGNL<data_t>::formatStep(const DataContainer<data_t>&) const
    {
        return fmt::format("{:>15.10} | {:>15.10} | {:>15.10}", delta_, beta_, alpha_);
    }

    template <typename data_t>
    CGNL<data_t>* CGNL<data_t>::cloneImpl() const
    {
        return new CGNL(*f_, *lineSearch_, beta_function_);
    }

    template <typename data_t>
    bool CGNL<data_t>::isEqual(const Solver<data_t>& other) const
    {
        auto otherCG = downcast_safe<CGNL>(&other);
        if (!otherCG)
            return false;

        if (epsilon_ != otherCG->epsilon_)
            return false;

        return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class CGNL<float>;
    template class CGNL<double>;
} // namespace elsa
