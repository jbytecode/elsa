#include "LandweberIteration.h"
#include "DataContainer.h"
#include "Functional.h"
#include "LinearOperator.h"
#include "Logger.h"
#include "TypeCasts.hpp"
#include "PowerIterations.h"
#include <iostream>

namespace elsa
{
    template <typename data_t>
    LandweberIteration<data_t>::LandweberIteration(const LinearOperator<data_t>& A,
                                                   const DataContainer<data_t>& b, data_t stepSize)
        : Solver<data_t>(), A_(A.clone()), b_(b), residual_(emptylike(b)), stepSize_{stepSize}
    {
    }

    template <typename data_t>
    LandweberIteration<data_t>::LandweberIteration(const LinearOperator<data_t>& A,
                                                   const DataContainer<data_t>& b)
        : Solver<data_t>(), A_(A.clone()), b_(b), residual_(emptylike(b))
    {
    }

    template <typename data_t>
    DataContainer<data_t> LandweberIteration<data_t>::setup(std::optional<DataContainer<data_t>> x0)
    {
        auto x = extract_or(x0, A_->getDomainDescriptor());

        if (!tam_) {
            tam_ = setupOperators(*A_);
        }

        if (!stepSize_.isInitialized()) {
            // Choose step length to be just below \f$\frac{2}{\sigma^2}\f$, where \f$\sigma\f$
            // is the largest eigenvalue of \f$T * A^T * M * A\f$. This is computed using the power
            // iterations.
            auto Anorm = powerIterations(*tam_ * *A_);
            stepSize_ = 0.9 * (2. / Anorm);
        }

        Logger::get("LandweberIterations")->info("Using Steplength: {}", *stepSize_);

        // setup done!
        this->configured_ = true;

        return x;
    }

    template <typename data_t>
    DataContainer<data_t> LandweberIteration<data_t>::step(DataContainer<data_t> x)
    {
        // Compute Ax - b memory efficient
        A_->apply(x, residual_);
        residual_ -= b_;

        x -= (*stepSize_) * tam_->apply(residual_);

        projection_(x);

        return x;
    }

    template <typename data_t>
    std::string LandweberIteration<data_t>::formatHeader() const
    {
        return fmt::format("{:^12} | {:^12} |", "x norm", "Residual");
    }

    template <typename data_t>
    std::string LandweberIteration<data_t>::formatStep(const DataContainer<data_t>& x) const
    {
        return fmt::format("{:>12.5f} | {:>12.5f} |", x.l2Norm(), residual_.l2Norm());
    }

    template <typename data_t>
    bool LandweberIteration<data_t>::isEqual(const Solver<data_t>& other) const
    {
        auto landweber = downcast_safe<LandweberIteration<data_t>>(&other);
        return landweber && *A_ == *landweber->A_ && b_ == landweber->b_
               && stepSize_ == landweber->stepSize_;
    }

    template <class data_t>
    void LandweberIteration<data_t>::setProjection(
        const std::function<void(DataContainer<data_t>&)> projection)
    {
        projection_ = projection;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class LandweberIteration<float>;
    template class LandweberIteration<double>;
} // namespace elsa
