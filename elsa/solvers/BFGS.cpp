#include "BFGS.h"
#include "Logger.h"

namespace elsa
{

    template <typename data_t>
    BFGS<data_t>::BFGS(const Functional<data_t>& problem,
                       const LineSearchMethod<data_t>& line_search_method, const data_t& tol)
        : Solver<data_t>(), _problem(problem.clone()), _ls(line_search_method.clone()), _tol{tol}
    {
        // sanity check
        if (tol < 0)
            throw InvalidArgumentError("BFGS: tolerance has to be non-negative");
    }

    template <typename data_t>
    DataContainer<data_t> BFGS<data_t>::solve(index_t iterations,
                                              std::optional<DataContainer<data_t>> x0)
    {
        auto xi = extract_or(x0, _problem->getDomainDescriptor());
        auto xi_1 = DataContainer<data_t>(_problem->getDomainDescriptor());
        auto gi = _problem->getGradient(xi);
        auto gi_1 = DataContainer<data_t>(_problem->getDomainDescriptor());
        auto n = xi.getSize();

        auto to_map = [](auto&& vect) -> Eigen::Map<Vector_t<data_t>> {
            return Eigen::Map<Vector_t<data_t>>(thrust::raw_pointer_cast(vect.storage().data()),
                                                vect.getSize());
        };

        auto H = Matrix_t<data_t>(n, n);
        auto I = Matrix_t<data_t>::Identity(n, n);

        auto di = -gi;

        xi_1 = xi;
        gi_1 = gi;
        xi += _ls->solve(xi, di) * di;
        gi = _problem->getGradient(xi);
        auto si = xi - xi_1;
        auto yi = gi - gi_1;
        H = yi.dot(si) / yi.dot(yi) * I;
        auto rho = 1 / yi.dot(si);
        Logger::get("BFGS")->info("iteration {} of {}", 1, iterations);

        for (index_t i = 1; i < iterations; ++i) {
            Logger::get("BFGS")->info("iteration {} of {}", i + 1, iterations);
            if (gi.l2Norm() < _tol) {
                return xi;
            }
            auto si_map = to_map(si);
            auto yi_map = to_map(yi);
            auto gi_map = to_map(gi);
            H = (I - rho * si_map * yi_map.transpose()) * H
                    * (I - rho * yi_map * si_map.transpose())
                + rho * si_map * si_map.transpose();
            di = DataContainer<data_t>{gi.getDataDescriptor(), -H * gi_map};
            xi_1 = xi;
            gi_1 = gi;
            xi += _ls->solve(xi, di) * di;
            gi = _problem->getGradient(xi);
            si = xi - xi_1;
            yi = gi - gi_1;
            rho = 1 / yi.dot(si);
        }

        return xi;
    } // namespace elsa

    template <typename data_t>
    BFGS<data_t>* BFGS<data_t>::cloneImpl() const
    {
        return new BFGS(*_problem, *_ls, _tol);
    }

    template <typename data_t>
    bool BFGS<data_t>::isEqual(const Solver<data_t>& other) const
    {
        auto otherBFGS = downcast_safe<BFGS<data_t>>(&other);
        if (!otherBFGS)
            return false;

        // TODO: compare line search methods
        return _tol == otherBFGS->_tol;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class BFGS<float>;
    template class BFGS<double>;
} // namespace elsa
