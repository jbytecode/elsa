#include "ProximalL2Squared.h"
#include "VolumeDescriptor.h"

#include "doctest/doctest.h"
#include <testHelpers.h>

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("proximal_operators");

TEST_CASE_TEMPLATE("ProximalL2: Testing regularity", data_t, float, double)
{
    static_assert(std::is_default_constructible_v<ProximalL2Squared<data_t>>);
    static_assert(std::is_copy_assignable_v<ProximalL2Squared<data_t>>);
    static_assert(std::is_copy_constructible_v<ProximalL2Squared<data_t>>);
    static_assert(std::is_nothrow_move_assignable_v<ProximalL2Squared<data_t>>);
    static_assert(std::is_nothrow_move_constructible_v<ProximalL2Squared<data_t>>);
}

TEST_CASE_TEMPLATE("ProximalL2: Testing without a given vector b", data_t, float, double)
{
    IndexVector_t numCoeff({{10}});
    VolumeDescriptor desc(numCoeff);

    Vector_t<data_t> xvec({{8.10295253, 8.93325664, 0.55758864, 9.11832862, 9.10185855, 8.03987632,
                            3.49846135, 0.55417797, 2.99925531, 5.49291213}});
    DataContainer<data_t> x(desc, xvec);

    ProximalL2Squared<data_t> prox;

    GIVEN("With a threshold parameter of 1")
    {
        auto result = prox.apply(x, 1);

        Vector_t<data_t> expectedVec(
            {{2.70098418, 2.97775221, 0.18586288, 3.03944287, 3.03395285, 2.67995877, 1.16615378,
              0.18472599, 0.99975177, 1.83097071}});
        DataContainer<data_t> expected(desc, expectedVec);

        CAPTURE(x);
        CAPTURE(expected);
        CAPTURE(result);

        for (int i = 0; i < expected.getSize(); ++i) {
            CHECK_UNARY(checkApproxEq(expected[i], result[i]));
        }
    }

    GIVEN("With a threshold parameter of 10")
    {
        auto result = prox.apply(x, 10);

        Vector_t<data_t> expectedVec({{0.38585488, 0.42539317, 0.02655184, 0.43420612, 0.43342184,
                                       0.38285125, 0.1665934, 0.02638943, 0.14282168, 0.26156724}});
        DataContainer<data_t> expected(desc, expectedVec);

        CAPTURE(x);
        CAPTURE(expected);
        CAPTURE(result);

        for (int i = 0; i < expected.getSize(); ++i) {
            CHECK_UNARY(checkApproxEq(expected[i], result[i]));
        }
    }
}

TEST_CASE_TEMPLATE("ProximalL2: Testing with a given vector b", data_t, float, double)
{
    IndexVector_t numCoeff({{10}});
    VolumeDescriptor desc(numCoeff);

    Vector_t<data_t> bvec({{0.68994793, 0.76165068, 0.85495963, 0.94187384, 0.55210654, 0.41897791,
                            0.38656914, 0.65864022, 0.78059884, 0.46041161}});
    DataContainer<data_t> b(desc, bvec);

    Vector_t<data_t> xvec({{8.10295253, 8.93325664, 0.55758864, 9.11832862, 9.10185855, 8.03987632,
                            3.49846135, 0.55417797, 2.99925531, 5.49291213}});
    DataContainer<data_t> x(desc, xvec);

    ProximalL2Squared<data_t> prox(b);

    GIVEN("With a threshold parameter of 1")
    {
        auto result = prox.apply(x, 1);

        Vector_t<data_t> expectedVec({{3.16094946, 3.48551933, 0.75583597, 3.66735877, 3.40202388,
                                       2.95927738, 1.42386654, 0.62381947, 1.520151, 2.13791178}});
        DataContainer<data_t> expected(desc, expectedVec);

        CAPTURE(b);
        CAPTURE(x);
        CAPTURE(expected);

        for (int i = 0; i < expected.getSize(); ++i) {
            CHECK_UNARY(checkApproxEq(expected[i], result[i]));
        }
    }

    GIVEN("With a threshold parameter of 10")
    {
        auto result = prox.apply(x, 10);

        Vector_t<data_t> expectedVec(
            {{1.04294815, 1.15077477, 0.84079911, 1.33122883, 0.95923759, 0.78187783, 0.53475448,
              0.65366583, 0.88624915, 0.70005449}});
        DataContainer<data_t> expected(desc, expectedVec);

        CAPTURE(b);
        CAPTURE(x);
        CAPTURE(expected);
        CAPTURE(result);

        for (int i = 0; i < expected.getSize(); ++i) {
            CHECK_UNARY(checkApproxEq(expected[i], result[i]));
        }
    }
}

TEST_SUITE_END();
