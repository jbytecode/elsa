#pragma once

#include "memory_resource/ContiguousVector.h"
#include "memory_resource/AllocationHint.h"
#include "memory_resource/CacheResource.h"
#include "memory_resource/PoolResource.h"
#include "memory_resource/RegionResource.h"
#include "memory_resource/LoggingResource.h"
#include "memory_resource/SyncResource.h"
#include "memory_resource/UniversalResource.h"
#include "memory_resource/HostStandardResource.h"

#include "DisableWarnings.h"

DISABLE_WARNING_PUSH
DISABLE_WARNING_SIGN_CONVERSION
#include <thrust/universal_vector.h>
DISABLE_WARNING_POP

namespace elsa
{
    /// @brief Represents the internal storage type used by the DataContainer.
    ///        It uses the contiguous vector with the uninitialized data tag, in order to
    ///        prevent unnecessary initializations. Iterator and pointer types are mapped
    ///        to thrust::universal_ptr. It inherits from the contiguous vector in order to
    ///        allow explicit constructors for ContiguousStorage to exist, in comparison to
    ///        having to use the ContigousVector constructor, when using
    ///        using ContiguousStorage = mr::ContiguousVector...
    /// @tparam T type of the data stored in the container.
    template <class T>
    class ContiguousStorage final
        : public mr::ContiguousVector<T, mr::type_tags::uninitialized, thrust::universal_ptr,
                                      thrust::universal_ptr>
    {
    public:
        using mr::ContiguousVector<T, mr::type_tags::uninitialized, thrust::universal_ptr,
                                   thrust::universal_ptr>::ContiguousVector;
    };
} // namespace elsa