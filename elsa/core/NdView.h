#pragma once

#include <thrust/complex.h>
#include "ContiguousStorage.h"
#include "Utilities/StridedIterator.h"
#include "elsaDefines.h"

DISABLE_WARNING_PUSH
DISABLE_WARNING_SIGN_CONVERSION
#include <thrust/universal_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/functional.h>
#include <thrust/transform.h>
#include <thrust/for_each.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/zip_iterator.h>
DISABLE_WARNING_POP

#include <memory>
#include <type_traits>
#include <cinttypes>
#include <functional>
#include <exception>

namespace elsa
{
    class NdViewDimError : public std::runtime_error
    {
    public:
        NdViewDimError() : std::runtime_error("NdView dimensions do not match") {}
    };
    class NdViewUnsupportedShape : public std::runtime_error
    {
    public:
        NdViewUnsupportedShape()
            : std::runtime_error("NdView one or more dimensions are not positive")
        {
        }
    };
    class NdViewDimOutOfBounds : public std::runtime_error
    {
    public:
        NdViewDimOutOfBounds() : std::runtime_error("NdView dimension out-of-bounds") {}
    };
    class NdViewIndexOutOfBounds : public std::runtime_error
    {
    public:
        NdViewIndexOutOfBounds() : std::runtime_error("NdView index into dimension out-of-bounds")
        {
        }
    };
    class NdViewUnavailableIteration : public std::runtime_error
    {
    public:
        NdViewUnavailableIteration()
            : std::runtime_error("NdView iteration range not available for current data")
        {
        }
    };
    class NdViewEmptyView : public std::runtime_error
    {
    public:
        NdViewEmptyView() : std::runtime_error("NdView is empty") {}
    };

    template <typename data_t, mr::StorageType indexed_tag, mr::StorageType index_tag>
    class BoolIndexedView;

    template <class data_t, mr::StorageType tag>
    class NdViewTagged;

    namespace detail
    {
        template <typename View, typename Functor>
        auto with_canonical_range(View& view, Functor&& f)
            -> decltype(std::declval<Functor>()(std::declval<View>().canonical_range()))
        {
            static_assert(std::is_same_v<decltype(std::declval<Functor>()(view.canonical_range())),
                                         decltype(std::declval<Functor>()(view.range()))>,
                          "Functor return value differs, depending on input iterator range "
                          "type!");
            if (view.is_canonical()) {
                return f(view.canonical_range());
            } else {
                return f(view.range());
            }
        }

        /* throws NdViewDimError if the dimension coefficions do not match */
        bool are_strides_compatible(const IndexVector_t& shape1, const IndexVector_t& strides1,
                                    const IndexVector_t& shape2, const IndexVector_t& strides2);

        template <typename size_type>
        std::pair<IndexVector_t, IndexVector_t>
            unpack_dim_data(const ContiguousStorage<DimData<size_type>>& shape)
        {
            size_t dim_count = shape.size();
            IndexVector_t out_shape(dim_count);
            IndexVector_t out_strides(dim_count);
            for (size_t i = 0; i < dim_count; i++) {
                auto dim_data = shape[i];
                out_shape(i) = dim_data.shape;
                out_strides(i) = dim_data.stride;
            }
            return std::make_pair(out_shape, out_strides);
        }

        template <typename data_t, mr::StorageType tag>
        NdViewTagged<data_t, tag> create_uninitialized_owned_view(const IndexVector_t& shape,
                                                                  const IndexVector_t& strides,
                                                                  size_t allocation_size)
        {
            auto memres = mr::defaultResource();
            if (std::numeric_limits<size_t>::max() / sizeof(data_t) < allocation_size) {
                throw std::runtime_error("Overflowing multiplication");
            }
            size_t out_data_size = sizeof(data_t) * allocation_size;
            void* out_data = memres->allocate(out_data_size, alignof(data_t));
            return NdViewTagged<data_t, mr::sysStorageType>(
                reinterpret_cast<data_t*>(out_data), shape, strides,
                [=]() { memres->deallocate(out_data, out_data_size, alignof(data_t)); });
        }

        template <typename data_t, typename Functor>
        struct binop_to_unop
            : public thrust::unary_function<data_t,
                                            decltype(std::declval<Functor>()(
                                                std::declval<data_t>(), std::declval<data_t>()))> {
            data_t other;
            Functor f;

            binop_to_unop(data_t other, Functor f) : other{other}, f{f} {}

            __host__ __device__ auto operator()(data_t e) const -> decltype(f(e, other))
            {
                return f(e, other);
            }
        };

    } // namespace detail

    /// @brief Represents a non-owning view. It can handle arbitrary strides (including negative).
    ///        Supports the creation of subviews. Supports iteration in canonical order with a
    ///        thrust::device compatible iterator, provided the storage type is device accessible.
    ///        Upon deletion, if no other NdView has a reference to the data, signals to the owner
    ///        of the data that it may be deleted via a destructor that is passed as
    ///        constructor parameter.
    ///        Additionally, NdViewTagged provides elementwise unary and binary operations and
    ///        filtered assignments.
    /// @tparam data_t type of the data that the NdView points to
    /// @tparam tag    storage type/location of the data; Unlike DataContainer, NdView supports
    ///                non-universal memory, if compiled with CUDA
    /// @see is_canonical()
    template <class data_t, mr::StorageType tag>
    class NdViewTagged
    {
    public:
        using pointer_type = std::conditional_t<
            tag == mr::StorageType::host, data_t*,
            std::conditional_t<tag == mr::StorageType::device, thrust::device_ptr<data_t>,
                               thrust::universal_ptr<data_t>>>;
        using const_pointer_type = std::conditional_t<
            tag == mr::StorageType::host, const data_t*,
            std::conditional_t<tag == mr::StorageType::device, thrust::device_ptr<const data_t>,
                               thrust::universal_ptr<const data_t>>>;

        using Scalar = data_t;
        using self_type = NdViewTagged<data_t, tag>;
        using value_type = data_t;
        using iterator = pointer_type;
        using const_iterator = const_pointer_type;
        using reference = data_t&;
        using const_reference = std::add_const_t<reference>;
        using dim_data_type = decltype(pointer_type() - pointer_type());
        using dim_data = DimData<dim_data_type>;

        struct Cleanup {
            std::function<void()> cleanup;
            Cleanup(std::function<void()>&& cleanup) : cleanup{cleanup} {}
            ~Cleanup() { cleanup(); }
        };

        template <class ItType>
        struct IteratorRange {
        private:
            ItType _begin = ItType();
            ItType _end = ItType();

        public:
            IteratorRange(const ItType& b, const ItType& e) : _begin(b), _end(e) {}
            ItType begin() const { return _begin; }
            ItType end() const { return _end; }
        };

    private:
        struct Container {
            std::shared_ptr<Cleanup> cleanup;
            pointer_type pointer = pointer_type();
            IndexVector_t shape;
            IndexVector_t strides;
            ContiguousStorage<dim_data> dims;
            pointer_type contiguous_start = pointer_type();
            size_t size{0};
            bool is_canonical = false;
            bool contiguous = false;
        };
        std::shared_ptr<Container> _container;

    public:
        /// @brief Create a view on raw (possibly non-contiguous) data
        /// @param cleanup shared encapsulated destructor; to be called, once this NdView
        ///                (and all of its parent or subviews) have been deleted
        NdViewTagged(data_t* raw_data, const IndexVector_t& shape, const IndexVector_t& strides,
                     const std::shared_ptr<Cleanup>& cleanup)
            : _container{std::make_shared<Container>()}
        {
            size_t dims = shape.size();
            if (static_cast<ssize_t>(dims) != strides.size())
                throw NdViewDimError();

            /* check that all shapes are positive and if the overall container describes an
             *  empty volume (initialize size as 1 as zero-dimensional objects have this size) */
            _container->size = 1;
            for (auto& x : shape) {
                if (x < 0)
                    throw NdViewUnsupportedShape();
                if (x == 0)
                    _container->size = 0;
            }
            if (_container->size == 0)
                dims = 0;
            _container->cleanup = cleanup;
            _container->pointer = pointer_type(raw_data);
            _container->shape = shape;
            _container->strides = strides;
            _container->dims = ContiguousStorage<dim_data>(static_cast<size_t>(dims));
            _container->contiguous_start = _container->pointer;
            _container->is_canonical = true;
            _container->contiguous = true;

            /* check if the container is empty or if it is zero-dimensional,
             * in which case the processing is done here */
            if (_container->size == 0)
                return;
            if (dims == 0)
                return;

            /* iterate over all dimensions and:
             *  - check if its a canonical layout
             *  - adjust the start-pointer to the actual first address
             *      to allow for contiugous iteration (in case of negative strides)
             *  - create the copy of the strides in order to determine if the range is contiguous */
            std::vector<dim_data> shapeAndStrides(dims);
            for (size_t i = 0; i < dims; i++) {
                _container->dims[i] = {shape(i), strides(i)};
                shapeAndStrides[i] = {shape(i), std::abs(strides(i))};

                /* check if the stride matches the canonical layout */
                if (static_cast<ssize_t>(_container->size) != _container->dims[i].stride)
                    _container->is_canonical = false;

                /* check if the stride is negative and adjust the pointer accordingly */
                if (_container->dims[i].stride < 0) {
                    /* move the adjusted pointer back by the negative stides by its dimension */
                    _container->contiguous_start +=
                        _container->dims[i].stride * _container->dims[i].shape;
                }
                _container->size *= shape(i);
            }

            /* sort the shapes and strides by the strides and check if every next stride
             *  is equal to the previous stride times shape (in which case its contiguous) */
            std::sort(shapeAndStrides.begin(), shapeAndStrides.end(),
                      [](auto& a, auto& b) { return a.stride < b.stride; });

            /* dims > 0 is ensured above */
            _container->contiguous = (shapeAndStrides[0].stride == 1);
            for (size_t i = 1; i < dims && _container->contiguous; i++) {
                _container->contiguous =
                    shapeAndStrides[i].stride
                    == shapeAndStrides[i - 1].stride * shapeAndStrides[i - 1].shape;
            }

            /* is_canonical will already be implicitly false, but this makes it clearer */
            if (!_container->contiguous)
                _container->is_canonical = false;
        }

        /// @brief Create a view on raw (possibly non-contiguous) data
        /// @param cleanup destructor to be called, once this NdView
        ///                (and all of its parent or subviews) have been deleted
        NdViewTagged(data_t* raw_data, const IndexVector_t& shape, const IndexVector_t& strides,
                     std::function<void()> destructor)
            : NdViewTagged(raw_data, shape, strides,
                           std::make_shared<Cleanup>(std::move(destructor)))
        {
        }

        /// @brief Create an empty view
        NdViewTagged() : _container{std::make_shared<Container>()}
        {
            _container->size = 0;
            _container->is_canonical = true;
            _container->contiguous = true;
            _container->cleanup = std::make_shared<Cleanup>([]() {});
        }

        /// @return true iff the raw data is layed out contiguously
        bool is_contiguous() const { return _container->contiguous; }
        /// @return true iff the raw data follows the canonical layout
        /// Canonical layout is defined as follows:
        /// strides[0] = 1;
        /// strides[i] = strides[i - 1] * shape[i - 1];
        /// It is tempting to call this column major layout, but there is one
        /// caveat. In elsa, the first index refers to the column (i.e. x-coordinate)
        /// and the second refers to the row (i.e. y-coordinate).
        bool is_canonical() const { return _container->is_canonical; }

        /// @return true iff the view does not contain any data
        bool is_empty() const { return _container->size == 0; }

        /// @brief iterate over the raw data in any order, if the data
        /// is contiguous (iterators are pointer)
        /// Useful for reductions or transformations where the order of the
        /// data is not relevant (strides are ignored).
        IteratorRange<pointer_type> contiguous_range()
        {
            if (!_container->contiguous)
                throw NdViewUnavailableIteration();
            return IteratorRange<pointer_type>(_container->contiguous_start,
                                               _container->contiguous_start + _container->size);
        }
        IteratorRange<const_pointer_type> contiguous_range() const
        {
            if (!_container->contiguous)
                throw NdViewUnavailableIteration();
            return IteratorRange<const_pointer_type>(
                _container->contiguous_start, _container->contiguous_start + _container->size);
        }

        /// @brief iterate over the raw data in canonical order, if the data
        /// is layed out in canonical layout (iterators are pointer).
        /// @see is_canonical()
        IteratorRange<pointer_type> canonical_range()
        {
            if (!_container->is_canonical)
                throw NdViewUnavailableIteration();
            return IteratorRange<pointer_type>(_container->contiguous_start,
                                               _container->contiguous_start + _container->size);
        }
        IteratorRange<const_pointer_type> canonical_range() const
        {
            if (!_container->is_canonical)
                throw NdViewUnavailableIteration();
            return IteratorRange<const_pointer_type>(
                _container->contiguous_start, _container->contiguous_start + _container->size);
        }

        /// @brief iterate over the data in canonical order, regardless of its real layout
        StridedRange<pointer_type> range()
        {
            return StridedRange<pointer_type>(_container->pointer, _container->size,
                                              _container->dims.data().get(),
                                              _container->dims.size());
        }
        StridedRange<const_pointer_type> range() const
        {
            return StridedRange<const_pointer_type>(_container->pointer, _container->size,
                                                    _container->dims.data().get(),
                                                    _container->dims.size());
        }

    private:
        template <class...>
        bool UnpackAndCheckBounds(size_t index) const
        {
            static_cast<void>(index);
            return true;
        }

        template <class Index, class... Indices>
        bool UnpackAndCheckBounds(size_t current, Index index, Indices... indices) const
        {
            if (index < 0 || index >= _container->shape(current))
                return false;
            return UnpackAndCheckBounds<Indices...>(current + 1, indices...);
        }

        template <class...>
        ssize_t UnpackAndComputeIndex(size_t index) const
        {
            static_cast<void>(index);
            return 0;
        }

        template <class Index, class... Indices>
        ssize_t UnpackAndComputeIndex(size_t current, Index index, Indices... indices) const
        {
            return (_container->strides(current) * index)
                   + UnpackAndComputeIndex<Indices...>(current + 1, indices...);
        }

        /// Performs an element-wise binary operation. The result is returned in
        /// a view, which owns a newly allocated buffer for the output. The strides
        /// of the output may not match the strides of either input.
        template <mr::StorageType other_tag, typename Functor>
        NdViewTagged<decltype(std::declval<Functor>()(std::declval<data_t>(),
                                                      std::declval<data_t>())),
                     mr::sysStorageType>
            binop(const NdViewTagged<data_t, other_tag>& other, Functor functor) const
        {
            static_assert(mr::are_storages_compatible<tag, other_tag>::value,
                          "Binary operations are only possible when both arguments live on "
                          "compatible devices");
            using output_type =
                decltype(std::declval<Functor>()(std::declval<data_t>(), std::declval<data_t>()));
            static_assert(
                std::is_trivially_copyable_v<output_type>,
                "Binary operations are only implemented for trivially copyable result types");

            const auto& shape = this->shape();
            const auto& strides = this->strides();
            const auto& other_shape = other.shape();
            const auto& other_strides = other.strides();

            size_t element_count = this->size();
            bool strides_compatible =
                detail::are_strides_compatible(shape, strides, other_shape, other_strides);

            IndexVector_t out_strides;

            bool same_layout = strides_compatible && this->is_contiguous() && other.is_contiguous();
            if (!same_layout) {
                /* layouts do not match or are non-contiguous, replace out strides with column major
                 * layout */
                size_t stride = 1;
                size_t dim_count = shape.size();
                out_strides = IndexVector_t(dim_count);
                for (size_t i = 0; i < dim_count; i++) {
                    out_strides(i) = stride;
                    stride *= shape(i);
                }
            } else {
                out_strides = strides;
            }

            /* No initialization is performed on the buffer before assigning its contents with
             * thrust::transform, so this only works for trivially copyable types. To extend it for
             * other types, one could use thrust::for_each with a functor that relies on placement
             * new to assign the computed values. */
            auto out = detail::create_uninitialized_owned_view<output_type, mr::sysStorageType>(
                shape, out_strides, element_count);

            auto out_range = out.contiguous_range();
            if (same_layout) {
                auto left_range = this->contiguous_range();
                auto right_range = other.contiguous_range();
                thrust::transform(left_range.begin(), left_range.end(), right_range.begin(),
                                  out_range.begin(), functor);
            } else {
                this->with_canonical_crange([&](auto left_range) mutable {
                    other.with_canonical_crange([&](auto right_range) mutable {
                        thrust::transform(left_range.begin(), left_range.end(), right_range.begin(),
                                          out_range.begin(), functor);
                    });
                });
            }
            return out;
        }

        /// Performs an element-wise unary operation. The result is returned in a view, which owns a
        /// newly allocated buffer for the output. The strides of the output may not match the
        /// strides of the input.
        template <typename Functor>
        NdViewTagged<decltype(std::declval<Functor>()(std::declval<data_t>())), mr::sysStorageType>
            unop(Functor functor) const
        {
            using output_type = decltype(std::declval<Functor>()(std::declval<data_t>()));
            static_assert(
                std::is_trivially_copyable_v<output_type>,
                "Binary operations are only implemented for trivially copyable result types");

            auto& shape = this->shape();
            IndexVector_t out_strides;
            size_t element_count = this->size();

            if (!is_contiguous()) {
                /* layout is non-contiguous, replace out strides with column major layout */
                size_t stride = 1;
                size_t dim_count = shape.size();
                out_strides = IndexVector_t(dim_count);
                for (size_t i = 0; i < dim_count; i++) {
                    out_strides(i) = stride;
                    stride *= shape(i);
                }
            } else {
                out_strides = this->strides();
            }

            auto out = detail::create_uninitialized_owned_view<output_type, mr::sysStorageType>(
                shape, out_strides, element_count);

            auto compute_functor = [=](auto src_range, auto dst_range) {
                thrust::transform(src_range.begin(), src_range.end(), dst_range.begin(), functor);
            };

            if (is_contiguous()) {
                auto src_range = contiguous_range();
                auto dst_range = out.contiguous_range();
                compute_functor(src_range, dst_range);
            } else {
                auto src_range = range();
                auto dst_range = out.range();
                compute_functor(src_range, dst_range);
            }
            return out;
        }

    public:
        template <class... Indices>
        const data_t& operator()(Indices... index) const
        {
            return const_cast<self_type*>(this)->operator()(index...);
        }

        /// @brief extract a single element; Indices for all dimensions must be supplied
        template <class... Indices>
        data_t& operator()(Indices... index)
        {
            if (sizeof...(Indices) != _container->shape.size())
                throw NdViewDimError();
            if (_container->size == 0)
                throw NdViewEmptyView();

            if constexpr (sizeof...(Indices) == 0)
                return _container->pointer[0];
            else {
                if (!UnpackAndCheckBounds<Indices...>(0, index...))
                    throw NdViewIndexOutOfBounds();
                return _container->pointer[UnpackAndComputeIndex<Indices...>(0, index...)];
            }
        }

        /// @brief returned NdView has its dimensionality reduce by one by
        ///        selecting a point along one dimension to bind to a set value
        /// @param dim dimension to fix
        /// @param where the index of the slice along the dimension
        self_type fix(size_t dim, size_t where)
        {
            if (dim >= static_cast<size_t>(_container->shape.size()))
                throw NdViewDimOutOfBounds();
            if (static_cast<dim_data_type>(where) >= _container->shape(dim))
                throw NdViewIndexOutOfBounds();

            /* allocate the new strides */
            IndexVector_t shape(_container->shape.size() - 1);
            IndexVector_t strides(_container->strides.size() - 1);
            for (index_t i = 0; i < _container->shape.size() - 1; ++i) {
                shape(i) = _container->shape(i + (i >= dim ? 1 : 0));
                strides(i) = _container->strides(i + (i >= dim ? 1 : 0));
            }

            /* adjust the pointer to the offset */
            data_t* raw =
                thrust::raw_pointer_cast(_container->pointer) + _container->strides(dim) * where;
            return self_type(raw, shape, strides, _container->cleanup);
        }

        /// @brief returned NdView has same dimensionality but a shape of less or
        ///        equal to the original along the corresponding dimension
        /// @param dim         dimension along which to take a sub-range
        /// @param where_begin lowest index along dimension dim (inclusive)
        /// @param where_end   highest index along dimension dim (exclusive);
        ///                    where_begin <= where_end must hold!
        self_type slice(size_t dim, size_t where_begin, size_t where_end)
        {
            if (dim >= _container->shape.size())
                throw NdViewDimOutOfBounds();
            if (where_begin >= static_cast<size_t>(_container->shape(dim))
                || where_end >= static_cast<size_t>(_container->shape(dim)))
                throw NdViewIndexOutOfBounds();
            if (where_begin == where_end)
                throw NdViewUnsupportedShape();

            data_t* raw = thrust::raw_pointer_cast(_container->pointer);
            size_t dims = _container->shape.size();
            IndexVector_t shape(dims);
            IndexVector_t strides(dims);
            for (size_t i = 0; i < dims; i++) {
                shape(i) = _container->shape(i);
                strides(i) = _container->strides(i);
            }

            /* limit the size along dimension dim */
            shape(dim) = where_end - where_begin;
            /* adjust start to skip all */
            raw += where_begin * strides(dim);

            return self_type(raw, shape, strides, _container->cleanup);
        }

        /// @return the shape of this NdView
        const IndexVector_t& shape() const { return _container->shape; }

        /// @return the strides of this NdView
        const IndexVector_t& strides() const { return _container->strides; }

        /// @return the shape and strides of this NdView; Stored in memory of type
        /// mr::sysStorageType, i.e. they are device accessible if compiled with CUDA
        const ContiguousStorage<dim_data>& layout_data() const { return _container->dims; }

        /// @return the cleanup sentinel; once its last reference is dropped, the destructor is
        /// called
        std::shared_ptr<Cleanup> getCleanup() { return _container->cleanup; }

        /// @return the number of elements in this view
        size_t size() const { return _container->size; }

        /// @brief Calls the functor with the lowest overhead iterator range that guarantees
        /// canonical iteration order. I.e. if the data is naturarlly layed out in canonical
        /// order, the iterators will be pointers.
        /// @param f functor to call with an iterator range object with methods .begin() and .end()
        template <typename Functor>
        auto with_canonical_range(Functor&& f)
            -> decltype(std::declval<Functor>()(this->canonical_range()))
        {
            detail::with_canonical_range(*this, std::forward<Functor>(f));
        }

        /// @brief Const version of with_canonical_range()
        /// @see with_canonical_range()
        template <typename Functor>
        auto with_canonical_crange(Functor&& f) const
            -> decltype(std::declval<Functor>()(this->canonical_range()))
        {
            detail::with_canonical_range(*this, std::forward<Functor>(f));
        }

        /// @brief Calls the functor with the lowest overhead iterator range available.
        /// I.e. if the data is naturarlly layed out in contiguously, the iterators will be
        /// pointers.
        /// @param f functor to call with an iterator range object with methods .begin() and .end()
        template <typename Functor>
        auto with_unordered_range(Functor f)
            -> decltype(std::declval<Functor>(this->contiguous_range()))
        {
            static_assert(std::is_same_v<decltype(std::declval<Functor>(this->contiguous_range())),
                                         decltype(std::declval<Functor>(this->range()))>,
                          "Functor return value differs, depending on input iterator range "
                          "type!");
            if (is_contiguous()) {
                return f(contiguous_range());
            } else {
                return f(range());
            }
        }

#define NDVIEW_BINOP(op)                                                                    \
    template <mr::StorageType other_tag>                                                    \
    auto operator op(const NdViewTagged<data_t, other_tag>& other)                          \
        const->decltype(binop(other, thrust::placeholders::_1 op thrust::placeholders::_2)) \
    {                                                                                       \
        return binop(other, thrust::placeholders::_1 op thrust::placeholders::_2);          \
    }                                                                                       \
                                                                                            \
    template <typename data2_t = data_t>                                                    \
    friend auto operator op(const NdViewTagged<data_t, tag>& ndview, data2_t other)         \
        ->typename std::enable_if<                                                          \
            std::is_same_v<data_t, data2_t>,                                                \
            NdViewTagged<decltype(std::declval<data_t>() op std::declval<data2_t>()),       \
                         mr::sysStorageType>>::type                                         \
    {                                                                                       \
        return ndview.unop(thrust::placeholders::_1 op other);                              \
    }                                                                                       \
                                                                                            \
    template <typename data2_t = data_t>                                                    \
    friend auto operator op(data2_t other, const NdViewTagged<data_t, tag>& ndview)         \
        ->typename std::enable_if<                                                          \
            std::is_same_v<data_t, data2_t>,                                                \
            NdViewTagged<decltype(std::declval<data2_t>() op std::declval<data_t>()),       \
                         mr::sysStorageType>>::type                                         \
    {                                                                                       \
        return ndview.unop(other op thrust::placeholders::_1);                              \
    }

        /* Elementwise binary operations. The result of the operation
         * is eagerly evaluated and returned in a new NdView that owns
         * the result.
         * Note assignment operators are not implemented, as their desired
         * semantics are unclear. One might expect that x *= 2 doubles
         * all elements of x in the underlying data that x is a view of.
         * However, then x = x * 2 would behave differently form x *= 2,
         * as x * 2 allocates a new buffer.
         **/
        NDVIEW_BINOP(+);
        NDVIEW_BINOP(-);
        NDVIEW_BINOP(*);
        NDVIEW_BINOP(/);
        NDVIEW_BINOP(%);
        NDVIEW_BINOP(>);
        NDVIEW_BINOP(<);
        NDVIEW_BINOP(>=);
        NDVIEW_BINOP(<=);
        NDVIEW_BINOP(==);
        NDVIEW_BINOP(!=);
        NDVIEW_BINOP(&);
        NDVIEW_BINOP(|);
        NDVIEW_BINOP(^);
        NDVIEW_BINOP(&&);
        NDVIEW_BINOP(||);

#undef NDVIEW_BINOP

#define NDVIEW_UNOP(op)                                             \
    auto operator op() const->decltype(op thrust::placeholders::_1) \
    {                                                               \
        return unop(op thrust::placeholders::_1);                   \
    }

        NDVIEW_UNOP(-);
        NDVIEW_UNOP(!);

        /// Creates a left hand side, to be used for filtered assignments.
        /// The index parameter must be an NdView of equal dimensions to this.
        /// The returned object can be assigned to, replacing all entries in this
        /// view, whose corresponding index element is true. Indices whose filter-
        /// element is false remain unchanged.
        /// If the index tensor does not have the correct dimensions, no exception
        /// is thrown until the actual assignment occurs.
        ///
        /// Example:
        /// ```
        /// template<typename T>
        /// void zero_value_range(NdViewTagged<float, T> &x, float lb, float ub) {
        ///     x[x >= lb && x < ub] = 0.0f;
        /// }
        /// ```
        /// This example function sets all elements in the value range lb <= x < ub
        /// to zero.
        /// @see BoolIndexedView
        template <mr::StorageType other_tag>
        BoolIndexedView<data_t, tag, other_tag>
            operator[](const NdViewTagged<bool, other_tag>& index)
        {
            return BoolIndexedView(*this, index);
        }
    };

    namespace detail
    {
        template <typename data_t>
        struct fill_const_if_functor
            : public thrust::unary_function<thrust::tuple<data_t&, bool>, void> {
            data_t fill_value;

            fill_const_if_functor(const data_t& fill_value) : fill_value{fill_value} {}

            __host__ __device__ void operator()(thrust::tuple<data_t&, bool> args)
            {
                if (thrust::get<1>(args)) {
                    thrust::get<0>(args) = fill_value;
                }
            }
        };

        template <typename data_t>
        struct fill_if_functor : public thrust::unary_function<thrust::tuple<data_t&, bool>, void> {
            __host__ __device__ void operator()(thrust::tuple<data_t&, bool, data_t> args)
            {
                if (thrust::get<1>(args)) {
                    thrust::get<0>(args) = thrust::get<2>(args);
                }
            }
        };

        template <mr::StorageType... tags>
        struct are_tags_host_compatible {
            static constexpr bool value = false;
        };

        template <mr::StorageType... tags>
        struct are_tags_host_compatible<mr::StorageType::universal, tags...> {
            static constexpr bool value = are_tags_host_compatible<tags...>::value;
        };

        template <mr::StorageType... tags>
        struct are_tags_host_compatible<mr::StorageType::host, tags...> {
            static constexpr bool value = are_tags_host_compatible<tags...>::value;
        };

        template <>
        struct are_tags_host_compatible<> {
            static constexpr bool value = true;
        };

        template <mr::StorageType... tags>
        struct select_policy {
        };

        template <>
        struct select_policy<> {
            static constexpr decltype(thrust::device) policy = thrust::device;
        };

        template <mr::StorageType... tags>
        struct select_policy<mr::StorageType::universal, tags...> {
            static constexpr decltype(select_policy<tags...>::policy) policy =
                select_policy<tags...>::policy;
        };

        template <mr::StorageType... tags>
        struct select_policy<mr::StorageType::host, tags...> {
            static_assert(are_tags_host_compatible<tags...>::value,
                          "Cannot select policy, as storage tags are incompatible");
            static constexpr decltype(thrust::host) policy = thrust::host;
        };
    } // namespace detail

    /// @brief NdView using the system storage type
    /// @tparam data_t type of the data that the NdView points to
    /// @see elsa::mr::sysStorageType
    template <class data_t>
    using NdView = NdViewTagged<data_t, mr::sysStorageType>;

    /// @brief Class representing a view with a boolean index tensor applied. Objects of this
    ///        class are intended to be assigned to, which overwrites the element whose filter is
    ///        true.
    template <typename data_t, mr::StorageType indexed_tag, mr::StorageType index_tag>
    class BoolIndexedView
    {
        NdViewTagged<data_t, indexed_tag> _indexed;
        NdViewTagged<bool, index_tag> _index;

    public:
        BoolIndexedView(const NdViewTagged<data_t, indexed_tag>& indexed,
                        NdViewTagged<bool, index_tag> index)
            : _indexed{indexed}, _index{index}
        {
        }

        /// @brief Assign the elements of rhs to the indexed tensor wherever the corresponding
        ///        element of the index tensor is true.
        /// @throw NdViewDimError if rhs, the left hand side NdView and the indexing tensor do
        ///        not all have the same dimensions.
        template <mr::StorageType rhs_tag>
        BoolIndexedView& operator=(const NdViewTagged<data_t, rhs_tag>& rhs)
        {
            static_assert(mr::are_storages_compatible<indexed_tag, rhs_tag>::value,
                          "Indexed view storage type and assignment right side storage type are "
                          "incompatible");
            static_assert(mr::are_storages_compatible<index_tag, rhs_tag>::value,
                          "Boolean index storage type and assignment right side storage type are "
                          "incompatible");

            const auto& indexed_shape = _indexed.shape();
            const auto& indexed_strides = _indexed.strides();
            const auto& index_shape = _index.shape();
            const auto& index_strides = _index.strides();
            const auto& rhs_shape = rhs.shape();
            const auto& rhs_strides = rhs.strides();
            bool strides_compatible = detail::are_strides_compatible(indexed_shape, indexed_strides,
                                                                     index_shape, index_strides)
                                      && detail::are_strides_compatible(
                                          indexed_shape, indexed_strides, rhs_shape, rhs_strides);

            auto copy_functor = [&](auto& indexed_range, auto& index_range, auto& rhs_range) {
                auto begin = thrust::make_zip_iterator(thrust::make_tuple(
                    indexed_range.begin(), index_range.begin(), rhs_range.begin()));
                auto end = thrust::make_zip_iterator(
                    thrust::make_tuple(indexed_range.end(), index_range.end(), rhs_range.end()));

                thrust::for_each(detail::select_policy<indexed_tag, index_tag, rhs_tag>::policy,
                                 begin, end, detail::fill_if_functor<data_t>());
            };

            if (strides_compatible && _indexed.is_contiguous() && _index.is_contiguous()
                && rhs.is_contiguous()) {
                auto indexed_range = _indexed.contiguous_range();
                auto index_range = _index.contiguous_range();
                auto rhs_range = rhs.contiguous_range();
                copy_functor(indexed_range, index_range, rhs_range);
            } else {
                _indexed.with_canonical_range([&](auto indexed_range) mutable {
                    _index.with_canonical_crange([&](auto index_range) mutable {
                        rhs.with_canonical_crange([&](auto rhs_range) mutable {
                            copy_functor(indexed_range, index_range, rhs_range);
                        });
                    });
                });
            }
            return *this;
        }

        /// @brief Assign rhs to the indexed tensor wherever the corresponding element of the index
        ///        tensor is true
        /// @throw NdViewDimError if the left hand side NdView and the indexing tensor do
        ///        not have the same dimensions.
        BoolIndexedView& operator=(data_t rhs)
        {
            const auto& indexed_shape = _indexed.shape();
            const auto& indexed_strides = _indexed.strides();
            const auto& index_shape = _index.shape();
            const auto& index_strides = _index.strides();
            bool strides_compatible = detail::are_strides_compatible(indexed_shape, indexed_strides,
                                                                     index_shape, index_strides);

            auto copy_functor = [&](auto indexed_begin, auto indexed_end, auto index_begin,
                                    auto index_end) {
                auto begin =
                    thrust::make_zip_iterator(thrust::make_tuple(indexed_begin, index_begin));
                auto end = thrust::make_zip_iterator(thrust::make_tuple(indexed_end, index_end));

                thrust::for_each(detail::select_policy<indexed_tag, index_tag>::policy, begin, end,
                                 detail::fill_const_if_functor(rhs));
            };

            if (strides_compatible && _indexed.is_contiguous() && _index.is_contiguous()) {
                auto indexed_range = _indexed.contiguous_range();
                auto index_range = _index.contiguous_range();
                copy_functor(indexed_range.begin(), indexed_range.end(), index_range.begin(),
                             index_range.end());
            } else {
                _indexed.with_canonical_range([&](auto indexed_range) {
                    _index.with_canonical_range([&](auto index_range) {
                        copy_functor(indexed_range.begin(), indexed_range.end(),
                                     index_range.begin(), index_range.end());
                    });
                });
            }
            return *this;
        }
    };
} // namespace elsa
