#pragma once

#include <cstdint>

namespace elsa
{
    /// FFTPolicy controls the choice of the fft backend.
    /// AUTO -> the implementation may choose any backend;
    /// DEVICE -> the cuFFT implementation must be used. An error is generated if this is not
    /// possible;
    /// HOST -> the Eigen based host sided implementation must be chosen.
    enum class FFTPolicy : uint8_t {
        AUTO,
        DEVICE,
        HOST,
    };
} // namespace elsa

#if __has_include("spdlog/fmt/fmt.h")
#include "spdlog/fmt/fmt.h"

template <>
struct fmt::formatter<elsa::FFTPolicy> {
    auto parse(format_parse_context& ctx) { return ctx.begin(); }

    template <typename FormatContext>
    auto format(const elsa::FFTPolicy& p, FormatContext& ctx) const
    {
        if (p == elsa::FFTPolicy::AUTO) {
            return fmt::format_to(ctx.out(), "AUTO");
        } else if (p == elsa::FFTPolicy::DEVICE) {
            return fmt::format_to(ctx.out(), "DEVICE");
        } else if (p == elsa::FFTPolicy::HOST) {
            return fmt::format_to(ctx.out(), "HOST");
        } else {
            return fmt::format_to(ctx.out(), "UNKNOWN VALUE");
        }
    }
};
#endif
