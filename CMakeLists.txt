cmake_minimum_required(VERSION 3.16 FATAL_ERROR)

project(
    elsa
    VERSION 0.8.1
    DESCRIPTION "elsa recon toolbox"
    LANGUAGES CXX
)

# detect if elsa is being as a submodule, enable/disable some options based on this
if(NOT DEFINED ELSA_MASTER_PROJECT)
    if(CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
        set(ELSA_MASTER_PROJECT ON)
    else()
        set(ELSA_MASTER_PROJECT OFF)
    endif()
endif()

# add our cmake modules under cmake/
set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})

# ------------ elsa configuration options ------------
# ------------

option(ELSA_TESTING "Enable building the unit tests" ${ELSA_MASTER_PROJECT})
option(ELSA_COVERAGE "Enable test coverage computation and reporting" OFF)

option(ELSA_INSTALL "Enable generating the install targets for make install" ${ELSA_MASTER_PROJECT})
option(ELSA_EXPORT_BUILD_TREE "Enable export target to consume projects build tree from outside projects"
       ${ELSA_MASTER_PROJECT}
)
option(ELSA_BUILD_EXAMPLES "Enable building of examples" ${ELSA_MASTER_PROJECT})
option(ELSA_BUILD_BENCHMARKS "Enable building of benchmarks" OFF)

option(ELSA_BUILD_PYTHON_BINDINGS "Attempt to build python bindings if enabled (requires libclang-dev)" ON)
option(ELSA_BINDINGS_IN_SINGLE_MODULE "Combine the bindings for all elsa modules in a single python module" OFF)

set(ELSA_CUFFT_CACHE_SIZE "1"
    CACHE STRING "Number of cached CuFFT plans, improving performance at the cost of GPU memory. 0 to disable caching"
)

set(ELSA_WARNING_LEVEL "base" CACHE STRING "Warning level with which elsa is build. Options are: base, all")
set(ELSA_SANITIZER
    ""
    CACHE
        STRING
        "Compile with a sanitizer. Options are: Address, Memory, MemoryWithOrigins, Undefined, Thread, Leak, 'Address;Undefined'"
)

set(ELSA_CUDA_ARCH_TYPE "native" CACHE STRING "Set CUDA architectures")
set(ELSA_ALTERNATIVE_LINKER ""
    CACHE STRING "Use alternate linker. Leave empty for system default; alternatives are 'gold', 'lld', 'bfd', 'mold'"
)

option(SYSTEM_EIGEN "Build elsa using the system eigen installation" OFF)
option(SYSTEM_SPDLOG "Build elsa using the system spdlog installation" OFF)
option(SYSTEM_DOCTEST "Build elsa using the system doctest installation" OFF)
option(SYSTEM_THRUST "Build elsa using the system thrust installation" OFF)

option(BUILD_SHARED_LIBS "Build elsa as shared (dynamic) libraries" OFF)

# optional features can be controlled with a variable named WANT_DEPENDENCYNAME. it can be controlled with three values:
#
# * ON: support is enabled and required
# * if_available: support is enabled, but skipped if not found
# * OFF: support is disabled

# default definitions for optional features
if(NOT DEFINED WANT_CUDA)
    set(WANT_CUDA if_available)
endif()

include(options)

# ------------ general setup -----------
# ------------

# set default built type to "Release" (if no other specified)
set(DEFAULT_BUILD_TYPE "Release")
include(SetDefaultBuildType)

# Forbid extensions (such as gnu++) for this project
set(CMAKE_CXX_EXTENSIONS OFF)

# export compile_commands.json for lsp language servers (e.g. clangd)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# By default set all output to lib or bin directories
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# set where to install the exports/targets
include(GNUInstallDirs)
include(CMakePackageConfigHelpers)
set(INSTALL_CONFIG_DIR ${CMAKE_INSTALL_LIBDIR}/cmake/elsa)

# Setup colored output for both ninja and Makefile generator, only introduced in CMake 3.24
if(${CMAKE_VERSION} VERSION_GREATER_EQUAL "3.24.0")
    set(CMAKE_COLOR_DIAGNOSTICS ON)
endif()

# ------------ Setup Linker ------------
# ------------

function(set_alternate_linker linker)
    find_program(LINKER_EXECUTABLE NAMES ld.${linker} ${linker})

    # TODO: For mold and GCC <= 12
    if(LINKER_EXECUTABLE)
        message(STATUS "Looking for linker '${linker}' - ${LINKER_EXECUTABLE}")
        if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" AND "${CMAKE_CXX_COMPILER_VERSION}" VERSION_LESS 12.1.0
           AND "${linker}" STREQUAL "mold"
        )
            message(WARNING "Alternative linker '${linker}' is not supported for GCC versions <= 12.1.0")
        else()
            add_link_options("-fuse-ld=${linker}")
        endif()
    else()
        message(STATUS "Alternative linker '${linker}' -- NOT FOUND")
        set(ELSA_ALTERNATIVE_LINKER
            ""
            CACHE STRING "Use alternate linker" FORCE
            PARENT_SCOPE
        )
    endif()
endfunction()

if(NOT "${ELSA_ALTERNATIVE_LINKER}" STREQUAL "")
    set_alternate_linker(${ELSA_ALTERNATIVE_LINKER})
endif()

# ------------ ccache compiler cache ------------
# ------------

# distros can also do this but they don't use this mechanism
option(ENABLE_CCACHE "prefix each compile command with ccache")

if(ENABLE_CCACHE)
    find_program(CCACHE_FOUND "ccache" REQUIRED)

    if(CCACHE_FOUND)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
    else()
        message(FATAL_ERROR "ccache not found, but you requested it")
    endif(CCACHE_FOUND)
endif()

# ------------ dependencies ------------
# ------------

# Enable CUDA support if requested
if(WANT_CUDA)
    message(STATUS "CUDA support requested...")
    include(CheckLanguage)
    check_language(CUDA)

    if(CMAKE_CUDA_COMPILER)
        set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} --expt-relaxed-constexpr --expt-extended-lambda")
        enable_language(CUDA)

        if(CMAKE_CUDA_COMPILER_VERSION VERSION_GREATER_EQUAL 11.6)
            find_package(CUDAToolkit 11.6 REQUIRED)

            macro(set_cuda_arch arch)
                if(${CMAKE_VERSION} VERSION_GREATER_EQUAL 3.24)
                    # see https://gitlab.kitware.com/cmake/cmake/-/issues/22375 and
                    set(CMAKE_CUDA_ARCHITECTURES ${arch})
                else()
                    # deal with CMake versions earlier than 3.24
                    set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} -arch=${arch}")
                endif()
            endmacro()

            if(ELSA_CUDA_ARCH_TYPE)
                set_cuda_arch(${ELSA_CUDA_ARCH_TYPE})
            elseif(NOT ${CMAKE_CUDA_ARCHITECTURES})
                message(
                    FATAL_ERROR
                        "No CUDA architecture specified, and CMake was unable to determine the default architecture."
                        "Consider setting ELSA_CUDA_ARCH_TYPE to 'native' or a list of valid architectures"
                        "Check the CMake documentation for CMAKE_CUDA_ARCHITECTURES and CUDA_ARCHITECTURES"
                )
            endif()

            # Activate unified memory
            add_definitions(-DELSA_HAS_CUDA_PROJECTORS)
            add_definitions(-DELSA_CUDA_UNIFIED_MEMORY)
            add_definitions(-DELSA_HAS_CUDA_PROJECTORS)
            set(ELSA_CUDA_UNIFIED_MEMORY ON)
            set(ELSA_CUDA_ENABLED ON)
            have_config_option(cuda CUDA true)

        else()
            message(STATUS "CUDA 11.5 or higher required, only ${CMAKE_CUDA_COMPILER_VERSION} found")
            have_config_option(cuda CUDA false)
        endif()
    else()
        message(STATUS "No CUDA compiler found, are you sure it's in your path? Try 'whereis nvcc'")
        have_config_option(cuda CUDA false)
    endif()
else()
    # Be sure everything is off
    have_config_option(cuda CUDA false)
endif()

if(NOT
   (SYSTEM_EIGEN
    AND SYSTEM_SPDLOG
    AND SYSTEM_DOCTEST
    AND SYSTEM_THRUST)
)
    # Include CPM dependency manager for bundling dependencies
    include(CPM)
endif()

# only add the dependencies if elsa is stand-alone
if(ELSA_MASTER_PROJECT)
    # Check if OpenMP is available
    find_package(OpenMP)

    include(Dependency)
    add_eigen()
    add_thrust()
    add_spdlog()

    if(ELSA_CUDA_ENABLED)
        find_package(CUDAToolkit)
        if(NOT CUDAToolkit_FOUND)
            message(STATUS "CUDA Toolkit not found")
        endif()
    endif()

    if(SKBUILD AND ELSA_BUILD_PYTHON_BINDINGS)
        # Scikit-Build does not add your site-packages to the search path automatically, so we need to add it _or_ the
        # pybind11 specific directory here.
        execute_process(
            COMMAND "${PYTHON_EXECUTABLE}" -c "import pybind11; print(pybind11.get_cmake_dir())"
            OUTPUT_VARIABLE _tmp_dir OUTPUT_STRIP_TRAILING_WHITESPACE
        )
        list(APPEND CMAKE_PREFIX_PATH "${_tmp_dir}")

        # Now we can find pybind11
        find_package(pybind11 CONFIG REQUIRED)

        add_subdirectory(pyelsa)
    endif()
else()
    message(STATUS "    No dependencies added for elsa, as it is not stand-alone")
endif()

# set where to install the exports/targets
include(GNUInstallDirs)
include(CMakePackageConfigHelpers)
set(INSTALL_CONFIG_DIR ${CMAKE_INSTALL_LIBDIR}/cmake/elsa)

# include the install_elsa_module function
include(InstallElsaModule)

# ------------ Setup Tools -----------
# ------------

# Includes sanitizers
include(Sanitizer)

# ------------ setup testing -----------
# ------------

# if elsa is used as a submodule, turn testing off
if(NOT ELSA_MASTER_PROJECT)
    set(ELSA_TESTING OFF)
endif(NOT ELSA_MASTER_PROJECT)

if(ELSA_TESTING)
    enable_testing()

    include(Dependency)
    add_doctest()

    message(STATUS "elsa testing is enabled")

    # Run ctest to run all unit tests All tests are includes ass dependencies to this target, such that they get build
    # schedule randomly that we don't start depending on some test order
    add_custom_target(
        tests
        COMMAND ${CMAKE_CTEST_COMMAND} --output-on-failure --schedule-random
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        USES_TERMINAL
        COMMENT "Build and run all the tests."
    )

    # Target to only build all tests
    add_custom_target(build-tests)

    if(ELSA_COVERAGE)
        message(STATUS "elsa test coverage is enabled")

        include(CodeCoverage)
        add_code_coverage_all_targets(
            EXCLUDE
            "elsa/test_routines/.*"
            ".*/tests/.*"
            "${PROJECT_BINARY_DIR}/_deps/.*"
            "${CPM_SOURCE_CACHE}/.*"
            "/usr/.*"
            "/opt/.*"
        )

    else(ELSA_COVERAGE)
        message(STATUS "elsa test coverage is disabled")
    endif(ELSA_COVERAGE)

else(ELSA_TESTING)
    message(STATUS "    elsa testing is disabled")
endif(ELSA_TESTING)

if(ELSA_BUILD_BENCHMARKS)
    message(STATUS "elsa benchmarks enabled")
    add_subdirectory(benchmark)
endif()
# ------------ add code/docs -----------
# ------------

# the elsa library
add_subdirectory(elsa)

# the documentation
add_subdirectory(docs EXCLUDE_FROM_ALL)

# the examples
if(ELSA_BUILD_EXAMPLES)
    add_subdirectory(examples EXCLUDE_FROM_ALL)
endif(ELSA_BUILD_EXAMPLES)

# ------------ setup installation ------
# ------------

# set up the target/library for make install
include(GNUInstallDirs)
include(CMakePackageConfigHelpers)

# setup the ElsaConfig*.cmake files
write_basic_package_version_file(
    ${CMAKE_BINARY_DIR}/elsa/elsaConfigVersion.cmake VERSION ${PROJECT_VERSION} COMPATIBILITY AnyNewerVersion
)
configure_package_config_file(
    ${CMAKE_CURRENT_LIST_DIR}/cmake/elsaConfig.cmake.in ${CMAKE_BINARY_DIR}/elsa/elsaConfig.cmake
    INSTALL_DESTINATION ${INSTALL_CONFIG_DIR}
)

if(ELSA_EXPORT_BUILD_TREE)
    # this puts the local build tree into the user package repository, but not the installed version...
    export(PACKAGE elsa)
endif()

if(ELSA_INSTALL)
    message(STATUS "setting up elsa installation")

    # install the config files
    install(FILES ${CMAKE_BINARY_DIR}/elsa/elsaConfig.cmake ${CMAKE_BINARY_DIR}/elsa/elsaConfigVersion.cmake
            DESTINATION ${INSTALL_CONFIG_DIR}
    )
endif(ELSA_INSTALL)

#
# Add uninstall target Remove all headers, library and CMake files, which where copied to the install location

add_custom_target(
    uninstall
    COMMAND ${CMAKE_COMMAND} -P "${PROJECT_SOURCE_DIR}/cmake/Uninstall.cmake"
    COMMENT "Uninstalling elsa (Removing all installed files)"
    DEPENDS "${CMAKE_CURRENT_BINARY_DIR}/install_manifest.txt"
)

# build information overview
message("
elsa configuration: |
--------------------+"
)

string(TOUPPER "CMAKE_CXX_FLAGS_${CMAKE_BUILD_TYPE}" BUILD_TYPE_CXX_FLAGS)
message(
    "            project | ${PROJECT_NAME}
           compiler | ${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION}
         build type | ${CMAKE_BUILD_TYPE}
           cxxflags | ${CMAKE_CXX_FLAGS}
   build type flags | ${${BUILD_TYPE_CXX_FLAGS}}
          build dir | ${CMAKE_BINARY_DIR}
     install prefix | ${CMAKE_INSTALL_PREFIX}"
)

if(ELSA_CUDA_ENABLED)
    get_property(cuda_architectures GLOBAL PROPERTY CUDA_ARCHITECTURES)
    message(
        "                    |
       CUDA config: |
      CUDA compiler | ${CMAKE_CUDA_COMPILER_ID} ${CMAKE_CUDA_COMPILER_VERSION}
         CUDA flags | ${CMAKE_CUDA_FLAGS}
 CUDA architectures | ${CMAKE_CUDA_ARCHITECTURES}"
    )
endif()

message("                    |
--------------------+"
)

print_config_options()

message(
    "                    |
   thrust backends: |
                C++ | ${THRUST_CPP_FOUND}
          Intel TBB | ${THRUST_TBB_FOUND}
             OpenMP | ${THRUST_OMP_FOUND}
               CUDA | ${THRUST_CUDA_FOUND}"
)

message("                    |
--------------------+"
)

set_property(GLOBAL PROPERTY TARGET_MESSAGES OFF)
