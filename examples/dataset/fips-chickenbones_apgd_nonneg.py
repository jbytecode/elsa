"""This is a reconstruction of the open-access chickenbone dataset provided by
the Finnish Inverse Problems Society.

The DOI of the data is 10.5281/zenodo.6986012. It can be found at
https://zenodo.org/record/6986012. The run the script you need to download
the zip files and extract them in a common folder.

A description taken from the FIPS website
(https://www.fips.fi/dataset.php#chickenbone) describing the dataset:
> This is an open-access dataset of a series of 3D cone-beam computed
> tomography scans (CBCT) of a chicken bone imaged at 4 different dose levels.
> The dose was changed by varying the X-ray tube current and detector exposure
> time appropriately.

In this example, the constrained optimization problem with the least squares
data term with a non-negativity constraint is solved using APGD/FISTA.
"""

import argparse
from pathlib import Path
import math

from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt
import numpy as np
import pyelsa as elsa
import tifffile
import tqdm

from fips_data import rebin2d, preprocess, load_dataset, define_sino_descriptor

# These might need to change depending on the scanning geometry!
sourceToDetector = 553.74
sourceToOrigin = 210.66
originToDetector = sourceToDetector - sourceToOrigin

detectorSpacing = np.asarray([0.050, 0.050])


def reconstruct(A, b, L=None, niters=10):
    h = elsa.IndicatorNonNegativity(A.getDomainDescriptor())
    if L is None:
        apgd = elsa.APGD(A, b, h)
    else:
        apgd = elsa.APGD(A, b, h, mu=1/L)

    return apgd.solve(niters)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="elsa Reconstruction of the open-access chickenbone dataset"
    )
    parser.add_argument("path", type=Path, help="Path to look for tif files")
    parser.add_argument(
        "-i",
        "--iters",
        type=int,
        default=5,
        help="Number of iterations for the reconstruction algorithm",
    )
    parser.add_argument(
        "-b",
        "--binning",
        type=int,
        default=2,
        help="Binning factor of preprocessing (needs to be a power of two)",
    )
    parser.add_argument(
        "-s", "--step", type=int, default=1, help="Take every n-th image only"
    )
    parser.add_argument("--padding", type=int, default=0,
                        help="Additional padding")
    parser.add_argument(
        "--resolution",
        type=float,
        default=1,
        help="Change the resolution of the reconstruction (default 1.0)",
    )

    args = parser.parse_args()

    if not elsa.cudaProjectorsEnabled():
        import warnings

        warnings.warn(
            "elsa is build without CUDA support. This reconstruction may take a long time."
        )

    binning = args.binning
    padding = args.padding
    resolution = args.resolution

    projections, angles = load_dataset(
        path=args.path, binning=binning, step=args.step, padding=padding, dataset="chicken", angle_factor=1.
    )

    # Compute magnification
    magnification = sourceToDetector / sourceToOrigin

    # Determine good size and spacing for volume
    vol_spacing = (detectorSpacing[0] * binning) / (magnification * resolution)
    size = int(
        math.ceil((np.max(projections.shape[:-1]) - padding) * resolution))

    volume_descriptor = elsa.VolumeDescriptor([size] * 3, [vol_spacing] * 3)

    # define the sinogram for elsa
    sinogram = define_sino_descriptor(
        projections, angles, volume_descriptor, binning * detectorSpacing, sourceToOrigin, originToDetector)
    sino_descriptor = sinogram.getDataDescriptor()

    # Set the forward and backward projector
    projector = elsa.JosephsMethodCUDA(volume_descriptor, sino_descriptor)

    # Perform reconstruction
    recon = reconstruct(
        projector,
        sinogram,
        niters=args.iters,
    )

    # Now just show everything...
    npreco = np.array(recon)

    # plt.tight_layout()
    fig = plt.figure(constrained_layout=True, figsize=(20, 10))
    fig1, fig2 = fig.subfigures(nrows=2, ncols=1)

    def add_colorbar(fig, ax, im):
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(im, cax=cax, orientation="vertical")

    ncols = 4

    axs = fig1.subplots(nrows=1, ncols=ncols)
    slices = np.arange(size // 3, 2 * size // 3, size // 3 / ncols, dtype=int)
    for ax, i in zip(axs, slices):
        im = ax.imshow(npreco[:, :, i], cmap="gray")
        # Hide ticks
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        ax.set_title(f"Slice {i}")
        add_colorbar(fig, ax, im)

    axs = fig2.subplots(nrows=1, ncols=ncols)
    for ax, i in zip(axs, slices):
        im = ax.imshow(npreco[:, i, :], cmap="gray")
        # Hide ticks
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)
        ax.set_title(f"Slice {i}")
        add_colorbar(fig, ax, im)
    plt.show()
