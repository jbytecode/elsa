#define ANKERL_NANOBENCH_IMPLEMENT

#include <nanobench.h>
#include "elsa.h"
#include "spdlog/fmt/fmt.h"

using namespace elsa;

template <typename T>
using storage = ContiguousStorage<T>;

template <mr::StorageType tag>
void bench_bool_indexing(ankerl::nanobench::Bench b, NdViewTagged<float, tag>& view)
{
    size_t element_count = view.size();
    b.run("Boolean Indexing", [&]() { view[view > 0.0f && view < element_count / 2] = 0.0; });

    ankerl::nanobench::doNotOptimizeAway(view);
}

struct LBFunctor {
    float lower_bound;
    LBFunctor(float lb) : lower_bound{lb} {}
    __host__ __device__ bool operator()(float f) const { return f > lower_bound; }
};

struct UBFunctor {
    float upper_bound;
    UBFunctor(float ub) : upper_bound{ub} {}
    __host__ __device__ bool operator()(float f) const { return f < upper_bound; }
};

struct AndFunctor {
    __host__ __device__ bool operator()(const thrust::tuple<bool, bool>& bools) const
    {
        return thrust::get<0>(bools) && thrust::get<1>(bools);
    }
};

struct SelectFunctor {
    __host__ __device__ float operator()(const thrust::tuple<float, bool, float>& args) const
    {
        if (thrust::get<1>(args)) {
            return thrust::get<2>(args);
        } else {
            return thrust::get<0>(args);
        }
    }
};

/* it seems thrust cannot figure out a policy for zip iterators
 * involving both host and device iterators */
template <mr::StorageType tag>
struct storage_type_to_thrust_policy {
    static constexpr decltype(thrust::device) policy = thrust::device;
};

template <>
struct storage_type_to_thrust_policy<mr::StorageType::host> {
    static constexpr decltype(thrust::host) policy = thrust::host;
};

struct AssignFunctor {
    __host__ __device__ void operator()(thrust::tuple<float&, bool> args) const
    {
        if (thrust::get<1>(args)) {
            thrust::get<0>(args) = 0.0;
        }
    };
};

template <mr::StorageType tag>
void bench_manual_bool_indexing(ankerl::nanobench::Bench b, NdViewTagged<float, tag>& view,
                                bool naive)
{
    size_t element_count = view.size();
    b.run(naive ? "Manual Boolean Indexing via Thrust (with temporaries, naive iterator)"
                : "Manual Boolean Indexing via Thrust (with temporaries, optimal iterator)",
          [&]() {
              auto to_benchmark = [=](auto range) {
                  storage<bool> bool_tensor1(element_count);
                  float lower_bound = 0.0f;
                  thrust::transform(range.begin(), range.end(), bool_tensor1.begin(),
                                    LBFunctor(lower_bound));

                  storage<bool> bool_tensor2(element_count);
                  float upper_bound = element_count / 2;
                  thrust::transform(range.begin(), range.end(), bool_tensor2.begin(),
                                    UBFunctor(upper_bound));

                  storage<bool> bool_tensor_combined(element_count);
                  auto bool_zip_begin = thrust::make_zip_iterator(
                      thrust::make_tuple(bool_tensor1.begin(), bool_tensor2.begin()));
                  auto bool_zip_end = thrust::make_zip_iterator(
                      thrust::make_tuple(bool_tensor1.end(), bool_tensor2.end()));
                  thrust::transform(storage_type_to_thrust_policy<tag>::policy, bool_zip_begin,
                                    bool_zip_end, bool_tensor_combined.begin(), AndFunctor());

                  auto zero_iterator = thrust::make_constant_iterator<float>(0.0f);
                  auto assign_if_begin = thrust::make_zip_iterator(thrust::make_tuple(
                      range.begin(), bool_tensor_combined.begin(), zero_iterator));
                  auto assign_if_end = thrust::make_zip_iterator(
                      thrust::make_tuple(range.end(), bool_tensor_combined.end(), zero_iterator));
                  thrust::transform(storage_type_to_thrust_policy<tag>::policy, assign_if_begin,
                                    assign_if_end, range.begin(), SelectFunctor());
              };

              if (naive) {
                  auto range = view.range();
                  to_benchmark(range);
              } else {
                  auto range = view.contiguous_range();
                  to_benchmark(range);
              }
          });
}

struct RangedAssignFunctor {
    float lb;
    float ub;
    float rhs;

    RangedAssignFunctor(float lb, float ub, float rhs) : lb{lb}, ub{ub}, rhs{rhs} {}

    __host__ __device__ void operator()(float& f) const
    {
        if (f >= lb && f < ub) {
            f = rhs;
        }
    }
};

template <mr::StorageType tag>
void bench_optimized_manual_bool_indexing(ankerl::nanobench::Bench b,
                                          NdViewTagged<float, tag>& view)
{
    size_t element_count = view.size();
    b.run("Manual Boolean Indexing via Thrust (no temporaries)", [&]() {
        storage<bool> bool_tensor1(element_count);
        auto range = view.contiguous_range();
        thrust::for_each(storage_type_to_thrust_policy<tag>::policy, range.begin(), range.end(),
                         RangedAssignFunctor(0.0, element_count / 2, 0.0));
    });
}

template <mr::StorageType tag>
void bench_lazy_manual_bool_indexing(ankerl::nanobench::Bench b, NdViewTagged<float, tag>& view)
{
    size_t element_count = view.size();
    b.run("Manual Boolean Indexing via Thrust (no temporaries, complex iterators)", [&]() {
        storage<bool> bool_tensor1(element_count);
        auto range = view.contiguous_range();
        auto lb_functor = LBFunctor(0.0);
        auto lb_begin = thrust::make_transform_iterator(range.begin(), lb_functor);
        auto lb_end = thrust::make_transform_iterator(range.end(), lb_functor);

        float ub = element_count / 2;
        auto ub_functor = UBFunctor(ub);
        auto ub_begin = thrust::make_transform_iterator(range.begin(), ub_functor);
        auto ub_end = thrust::make_transform_iterator(range.end(), ub_functor);

        auto and_functor = AndFunctor();
        auto range_check_begin = thrust::make_transform_iterator(
            thrust::make_zip_iterator(thrust::make_tuple(lb_begin, ub_begin)), and_functor);
        auto range_check_end = thrust::make_transform_iterator(
            thrust::make_zip_iterator(thrust::make_tuple(lb_end, ub_end)), and_functor);

        auto begin =
            thrust::make_zip_iterator(thrust::make_tuple(range.begin(), range_check_begin));
        auto end = thrust::make_zip_iterator(range.end(), range_check_end);
        thrust::for_each(storage_type_to_thrust_policy<tag>::policy, begin, end, AssignFunctor());
    });
}

#ifdef ELSA_CUDA_ENABLED
__global__ void assign_all_in_value_range(float* data, size_t count, float min, float max,
                                          float new_value)
{
    size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < count && data[i] > min && data[i] <= max) {
        data[i] = new_value;
    }
}

void bench_kernel_boolean_indexing(ankerl::nanobench::Bench b, thrust::universal_ptr<float> ptr,
                                   size_t element_count)
{
    b.run("Manual Boolean Indexing (kernel)", [&]() {
        assign_all_in_value_range<<<(element_count + 255) / 256, 256>>>(
            ptr.get(), element_count, 0.0f, element_count / 2, 0.0f);
        cudaDeviceSynchronize();
    });
}

__global__ void lb_kernel(float* in_data, bool* out_data, size_t count, float min)
{
    size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < count) {
        out_data[i] = in_data[i] > min;
    }
}

__global__ void ub_kernel(float* in_data, bool* out_data, size_t count, float max)
{
    size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < count) {
        out_data[i] = in_data[i] <= max;
    }
}

__global__ void and_kernel(bool* in_data1, bool* in_data2, bool* out_data, size_t count)
{
    size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < count) {
        out_data[i] = in_data1[i] && in_data2[i];
    }
}

__global__ void cond_assign_kernel(bool* in_data, float* out_data, size_t count, float rhs)
{
    size_t i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < count && in_data[i]) {
        out_data[i] = rhs;
    }
}

void bench_multiple_kernel_boolean_indexing(ankerl::nanobench::Bench b,
                                            thrust::universal_ptr<float> ptr, size_t element_count,
                                            bool sync)
{
    b.run(sync ? "Manual Boolean Indexing (multiple kernels, sync)"
               : "Manual Boolean Indexing (multiple kernels, no sync)",
          [&]() {
              storage<bool> lb_bools(element_count);
              lb_kernel<<<(element_count + 255) / 256, 256>>>(ptr.get(), lb_bools.data().get(),
                                                              element_count, 0.0);
              if (sync)
                  cudaDeviceSynchronize();
              storage<bool> ub_bools(element_count);
              ub_kernel<<<(element_count + 255) / 256, 256>>>(ptr.get(), ub_bools.data().get(),
                                                              element_count, element_count / 2);
              if (sync)
                  cudaDeviceSynchronize();
              storage<bool> and_bools(element_count);
              and_kernel<<<(element_count + 255) / 256, 256>>>(
                  lb_bools.data().get(), ub_bools.data().get(), and_bools.data().get(),
                  element_count);
              if (sync)
                  cudaDeviceSynchronize();
              cond_assign_kernel<<<(element_count + 255) / 256, 256>>>(
                  lb_bools.data().get(), ptr.get(), element_count, 0.0);
              cudaDeviceSynchronize();
          });
}
#endif

template <mr::StorageType tag>
NdViewTagged<float, tag> create_example_data(storage<float>& data, IndexVector_t shape)
{
    size_t element_count = 1;
    IndexVector_t strides(shape.size());
    for (ssize_t i = 0; i < shape.size(); element_count *= shape(i), i++) {
        strides(i) = element_count;
    }

    data.resize(element_count);
    float* raw_data = thrust::raw_pointer_cast(data.data());

    NdViewTagged<float, tag> view(raw_data, shape, strides, [&]() {});
    for (size_t i = 0; i < element_count; i++) {
        data[i] = i % 2 == 0 ? i : -i;
    }
    return view;
}

template <mr::StorageType tag>
void run_benchmarks()
{
    ankerl::nanobench::Bench b;
    b.title(tag == mr::StorageType::host ? "Boolean Indexing (host)" : "Boolean Indexing (device)");
    b.performanceCounters(true);
    b.minEpochIterations(100);

    IndexVector_t shape(2);
    shape << 512, 512;
    size_t element_count = shape.prod();

    auto with_example_data = [&](auto closure) {
        mr::hint::ScopedMR mr(mr::CacheResource::make());
        storage<float> data;
        auto view = create_example_data<tag>(data, shape);
        closure(view, data);
    };

    with_example_data([&](auto& view, auto& data) {
        static_cast<void>(data);
        bench_bool_indexing(b, view);
    });
    with_example_data([&](auto& view, auto& data) {
        static_cast<void>(data);
        bench_manual_bool_indexing(b, view, false);
    });
    with_example_data([&](auto& view, auto& data) {
        static_cast<void>(data);
        bench_manual_bool_indexing(b, view, true);
    });
    if constexpr (tag == mr::StorageType::universal) {
        with_example_data([&](auto& view, auto& data) {
            bench_kernel_boolean_indexing(b, data.data(), element_count);
        });
        with_example_data([&](auto& view, auto& data) {
            bench_multiple_kernel_boolean_indexing(b, data.data(), element_count, false);
        });
        with_example_data([&](auto& view, auto& data) {
            bench_multiple_kernel_boolean_indexing(b, data.data(), element_count, true);
        });
    }
    with_example_data(
        [&](auto& view, auto& data) { bench_optimized_manual_bool_indexing(b, view); });
    with_example_data([&](auto& view, auto& data) { bench_lazy_manual_bool_indexing(b, view); });
}

int main()
{
    run_benchmarks<mr::StorageType::host>();
    if constexpr (mr::sysStorageType == mr::StorageType::universal) {
        run_benchmarks<mr::StorageType::universal>();
    }
    return 0;
}