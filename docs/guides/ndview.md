# NdView
The `NdView` is a lightweight wrapper class of n-dimensional data of a (more or less) arbitrary shape. It solely contains a reference to the data, a description of the shape, and a functor to mark the data as released. This allows for flexible ownership semantics. An `NdView` may be the sole owner of its data, share it with other `NdView`s or reference data owned outside of the elsa framework.  It is fully interoparable with the `DataContainer`, which can both create an `NdView` of itself, as well as materialize an incoming `NdView`.

## Assignment
Given two `NdView`s `a` and `b`, the result of an assignment `a = b;` is that both `NdView`s now point to the same data, not that the elements of `a` are assigned with the values from `b`.

## Iterators and Layouts
The `NdView` offers functions to query information about the shape of the data it holds, read values at explicit indices, as well as to iterate the data. Here, it offers either efficient element-wise iteration, if the data are contiguous and the order is irrelevant, or ordered iteration which mimics elsa's row-major data layout. For the ordered iteration, there exist two possible iterators. The cheaper raw iterator, provided the data layout matches the internal data layout of elsa, or the more expensive strided range iterator, designed to traverse arbitrary shapes. When iterating over the `NdView`'s data in an order-agnostic algorithm, the contiguous range iterator should be preferred for performance reasons, provided the actual data layout permits this. Similarly, the ordered range iterator should be chosen instead of the strided range iterator. While its ability to hold data with dynamic layouts is the core strength of the `NdView`, this makes it impossible to statically determine the appropriate iterator type. To spare users from having to write cumbersome case distinctions, the `with_canonical_range` method exists, which takes in a callable that is dynamically called with the appropriate iterator range. This callable can be conveniently specified as a generic lambda.

## Slicing and Fixing Dimensions
The `NdView` offers functions to spawn new `NdView`s onto the same data, using different shapes. Specifically, the given shape can be sliced along any axis (`slice`), thereby preserving its dimensionality, or any axis can be fixed to a given index (`fix`), thereby reducing the dimensionality of the view by one.

## Unary and Binary Operators
The `NdView` offers element-wise binary operations with another `NdView` of equal dimensions, as well as binary operations with scalars. As the element type of `Ndviews` is not restricted to numbers, boolean operations or comparison operations that yield boolean tensors are also implemented. Unary arithmetic and logical negation operators are also provided.

***WARNING!*** Please note that the result of these operations is currently eagerly evaluated and stored in a new tensor. Because of this, `a = a + b;` does not lead to the elements of `a` being incremented by `b`, but instead `a` now points to a new tensor. To avoid confusion in this matter, only the simple assignment operator is overloaded, as otherwise `a = a + b;` and `a += b;` would behave differently.

```
NdView<float> a = ...;
NdView<float> b = a;
assert(a(1, 2, 3) == b(1, 2, 3));
a = a * 2;
assert(a(1, 2, 3) == b(1, 2, 3)); // FAILS
```

## Boolean Assignment
The `NdView` also provides an indexing operator that takes an `NdView` with `bool` elements. This argument must have the same dimensions as the indexee. The result is an object that can be assigned to, setting all elements of the indexee where the index is `true`. The right hand side of such an assignment may either be a scalar value or another `NdView` with the same element type as the indexee.

By combining the arithmetic, comparison and indexing operators, it is possible to conveniently specify composed operations, such as assigning all tensor elements in a given range:
```
void setValuesInRange(NdView<float> &tensor, float min, float max, float newValue) {
    tensor[tensor >= min && tensor <= max] = newValue;
}
```

## Examples
For examples, please see `examples/misc/ndview_ops.cpp`.
